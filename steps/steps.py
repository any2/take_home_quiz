from behave import given, when, then, step
import requests

api_endpoints = {}
request_headers = {}
response_codes = {}
response_texts = {}
request_bodies = {}
api_url = "http://localhost:8080/v1/cleaning-sessions"


@given(u'I set the base url')
def step_impl(context):
    global api_url
    api_url = "http://localhost:8080/v1/"


# START POST Scenario
@given(u'I Set POST posts api endpoint')
def step_impl(context):
    api_endpoints['POST_URL'] = api_url + 'cleaning-sessions'
    print('url :' + api_endpoints['POST_URL'])


@when(u'I Set HEADER param request content type as "{header_conent_type}"')
def step_impl(context, header_conent_type):
    request_headers['Content-Type'] = header_conent_type


@when(u'Set example request')
def step_impl(context):
    request_bodies['POST'] = {"roomSize": [5, 5], "coords": [1, 2], "patches": [[1, 0], [2, 2], [2, 3]],
                              "instructions": "NNESEESWNWW"}


@when(u'Set empty request')
def step_impl(context):
    request_bodies['POST'] = {"roomSize": None, "coords": None, "patches": None,
                              "instructions": None}


@when(u'Set every coordinate has a Patch')
def step_impl(context):
    request_bodies['POST'] = {"roomSize": [5, 5], "coords": [0, 0],
                              "patches": [[1, 0], [1, 1], [1, 2], [1, 3], [1, 4], [2, 0],
                                          [2, 1], [2, 2], [2, 3], [2, 4], [3, 0], [3, 1],
                                          [3, 2], [3, 3], [3, 4], [4, 0], [4, 1], [4, 2],
                                          [4, 3], [4, 4], [0, 1], [0, 2], [0, 3], [0, 4]],
                              "instructions": "NNNNESSSSENNNNESSSSENNNN"}


@when(u'Set Rectangular Room Size with 1 patch')
def step_impl(context):
    request_bodies['POST'] = { "roomSize" : [5, 6], "coords" : [2, 3], "patches" : [[2, 5]], "instructions" : "SEENNNNWWSWWEEN" }


@when(u'Send POST HTTP request')
def step_impl(context):
    response = requests.post(url=api_endpoints['POST_URL'], json=request_bodies['POST'], headers=request_headers)
    response_texts['POST'] = response.text
    print("post response :" + response.text)
    statuscode = response.status_code
    response_codes['POST'] = statuscode


@then(u'I receive valid HTTP response code 200')
def step_impl(context):
    print('Post rep code ;' + str(response_codes['POST']))
    assert response_codes['POST'] == 200


@then(u'I receive internal server error code 500')
def step_impl(context):
    print('Post rep code ;' + str(response_codes['POST']))
    assert response_codes['POST'] == 500


@then(u'Response BODY "{request_name}" finished coords is 1,3 with 1 patch cleaned')
def step_impl(context, request_name):
    print('request_name: ' + request_name)
    print(response_texts)
    assert response_texts[request_name] == '{"coords":[1,3],"patches":1}'


@then(u'Response BODY "{request_name}" finished coords is 4,4 with 24 patches cleaned')
def step_impl(context, request_name):
    print('request_name: ' + request_name)
    print(response_texts)
    assert response_texts[request_name] == '{"coords":[4,4],"patches":24}'


@then(u'Response BODY "{request_name}" finished coords is 2,5 with 1 patch cleaned')
def step_impl(context, request_name):
    print('request_name: ' + request_name)
    print(response_texts)
    assert response_texts[request_name] == '{"coords":[2,5],"patches":1}'


