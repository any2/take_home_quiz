import requests
import json

"""
This is just a file to verify the json results from the server.
"""

base_url = "http://localhost:8080/v1/cleaning-sessions"

headers = {
    'Content-Type': 'application/json',
}

data = '{ "roomSize" : [5, 6], "coords" : [2, 3], "patches" : [[2, 5]], "instructions" : "SEENNNNWWSWWEEN" }'

response = requests.post(base_url, headers=headers, data=str(data))


x = 2
y = 3
index = 0
data_json = json.loads(data)
cleaned_squares = []

for char in data_json['instructions']:
    print([x, y])
    print(char)
    if char == "E":
        x += 1
    if char == "W":
        x -= 1
    if char == "N":
        y += 1
    if char == "S":
        y -= 1
    if y >= 5:
        y = 5
    if x >= 5:
        x = 5
    for coords in data_json['patches']:
        if [x, y] == coords:
            if [x, y] not in cleaned_squares:
                cleaned_squares.append([x, y])
                index += 1
print(f"patches= {index} {[x, y]}")


print(response.json())