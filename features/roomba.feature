Feature: Test roomba application rest endpoints

Background:
    Given I set the base url

# Fails after the tests were run a few times saying eight patches were cleaned
Scenario: Example scenario from repo
    Given I Set POST posts api endpoint
   When I set HEADER param request content type as "application/json"
        And Set example request
        And Send POST HTTP request
   Then I receive valid HTTP response code 200
        And Response BODY "POST" finished coords is 1,3 with 1 patch cleaned

Scenario: Every coordinate has a patch
    Given I Set POST posts api endpoint
   When I set HEADER param request content type as "application/json"
        And Set every coordinate has a Patch
        And Send POST HTTP request
   Then I receive valid HTTP response code 200
        And Response BODY "POST" finished coords is 4,4 with 24 patches cleaned

Scenario: Empty request sent
    Given I Set POST posts api endpoint
   When I set HEADER param request content type as "application/json"
        And Set empty request
        And Send POST HTTP request
   Then I receive internal server error code 500

# Failing with 9 patches returned
Scenario: Rectangular Room Size with 1 patch
    Given I Set POST posts api endpoint
   When I set HEADER param request content type as "application/json"
        And Set Rectangular Room Size with 1 patch
        And Send POST HTTP request
   Then I receive valid HTTP response code 200
        And Response BODY "POST" finished coords is 2,5 with 1 patch cleaned
