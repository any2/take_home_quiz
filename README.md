# Take Home Assessment

Project is written in python3 so please make sure to have it installed on your machine.

Start the test application before you run the tests.

Steps to run the tests:
1. Navigate to the project folder in your terminal
2. Run the command `pip3 install -r requirements.txt` to install dependencies
3. Run command `behave`

If you have any issues please reach out to me. This was finished late at night so I might have missed something.